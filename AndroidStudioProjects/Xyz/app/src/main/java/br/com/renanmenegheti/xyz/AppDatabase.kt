package br.com.renanmenegheti.xyz

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = arrayOf(Pessoa::class, Workspace::class),  version = 2)
public abstract class AppDatabase: RoomDatabase() {

    companion object {

        private val NOME_DB = "pessoa.db"
        private var instance: AppDatabase? = null

        private fun create(context: Context): AppDatabase?{

            return Room.databaseBuilder(context, AppDatabase::class.java, NOME_DB).build()

        }


        public fun getInstance(context: Context): AppDatabase{

            if (instance == null){
                instance = create(context)
            }

            return instance!!

        }
    }


    public abstract fun pessoaDao(): PessoaDao
    public abstract fun workspaceDao(): WorkspaceDao


}