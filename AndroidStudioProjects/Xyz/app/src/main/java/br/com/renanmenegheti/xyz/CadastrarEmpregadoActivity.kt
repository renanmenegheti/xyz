package br.com.renanmenegheti.xyz

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_cadastrar_empregado.*
import kotlinx.android.synthetic.main.activity_cadastro.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class CadastrarEmpregadoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cadastrar_empregado)


        val myStrings = arrayOf("Presencial", "Home Office")

        var localidade = "YiKeS"

        val idWorkspace = intent.getIntExtra("idWorkspace", -1)

        Log.e("idWrksCadEmpregado:", idWorkspace.toString())

        spinnerLocalidade.adapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, myStrings)
        spinnerLocalidade.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                localidade = myStrings[position]


            }


        }

        btnCadastrarEmpregado.setOnClickListener{

            val pessoaDao = AppDatabase.getInstance(this).pessoaDao()

            var pessoa = Pessoa()




            doAsync {

                if (pessoaDao.getPessoaByEmail(edtEmailCadastroEmpregado.text.toString()) != null){
                    pessoa = pessoaDao.getPessoaByEmail(edtEmailCadastroEmpregado.text.toString())

                    uiThread {


                        Log.e("Pessoa por enqt: ", pessoa.toString())
                        pessoa.localidade = localidade
                        pessoa.cargo = edtCargoCadastroEmpregado.text.toString()
                        pessoa.horasSemanais = edtHorasSemanaisCadastroEmpregado.text.toString()
                        pessoa.idWorkspaceQueTrabalha = idWorkspace




                        doAsync {

                            pessoaDao.insert(pessoa)

                            uiThread {

                                finish()

                            }

                        }

                    }

                }



            }

        }

    }
}
