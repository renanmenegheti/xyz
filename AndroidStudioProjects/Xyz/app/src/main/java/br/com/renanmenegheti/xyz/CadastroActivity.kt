package br.com.renanmenegheti.xyz

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_cadastro.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class CadastroActivity : AppCompatActivity() {


    var pessoaTipo: String = "xd"

    val myStrings = arrayOf("fisica", "juridica", "meme")



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cadastro)




        spinnerTipoPessoa.adapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, myStrings)
        spinnerTipoPessoa.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                pessoaTipo = myStrings[position]


            }


        }


        btnCadastrarCadastro.setOnClickListener{



            val pessoa = carregaObjetoPessoa()

            onSalvaPessoa(this, pessoa)



        }




    }

    private fun carregaObjetoPessoa(): Pessoa {

        val pessoa = Pessoa()

        pessoa.tipo = pessoaTipo
        pessoa.email = edtEmailCadastro.text.toString()
        pessoa.nome = edtNomeCadastro.text.toString()
        pessoa.senha = edtSenhaCadastro.text.toString()
        pessoa.idWorkspaceQueTrabalha = 0
        pessoa.cargo = "que?"
        pessoa.horasSemanais = "0"
        pessoa.localidade = "onde?"

        return pessoa



    }

    fun onSalvaPessoa(context: Context, pessoa: Pessoa) {

        val pessoaDao = AppDatabase.getInstance(context).pessoaDao()

        doAsync {
            pessoaDao.insert(pessoa)

            uiThread {

                Toast.makeText(it, "Salvado memo", Toast.LENGTH_LONG).show()
                Log.e("salvado memo", "salvado memo")

            }
        }

    }
}
