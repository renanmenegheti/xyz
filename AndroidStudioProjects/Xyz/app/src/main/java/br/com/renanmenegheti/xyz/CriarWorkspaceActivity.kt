package br.com.renanmenegheti.xyz

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_criar_workspace.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class CriarWorkspaceActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_criar_workspace)

        Log.e("idpessoa",intent.getIntExtra("idPessoa", -1).toString())

        btnCriarWorkspace.setOnClickListener() {

            val workspaceDao = AppDatabase.getInstance(this).workspaceDao()

            val workspace = Workspace(
                nomeWorkspace = edtNomeWorkspace.text.toString(),
                endereco = edtEnderecoWorkspace.text.toString(),
                donoWorkspace = 1
            )



            doAsync {

                workspaceDao.insert(workspace)


                uiThread {

                    Toast.makeText(it, "inserido memo", Toast.LENGTH_SHORT).show()
                    finish()

                }


            }

        }
    }
}
