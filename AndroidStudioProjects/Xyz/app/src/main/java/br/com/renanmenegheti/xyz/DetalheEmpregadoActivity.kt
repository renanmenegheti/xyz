package br.com.renanmenegheti.xyz

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_cadastrar_empregado.*
import kotlinx.android.synthetic.main.activity_detalhe_empregado.*
import kotlinx.android.synthetic.main.activity_ver_empregados.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class DetalheEmpregadoActivity : AppCompatActivity() {


    var myStrings = arrayOf("Presencial", "Home Office")
    var localidade = "ta em choKK??"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalhe_empregado)

        val idEmpregado = intent.getIntExtra("idPessoa", -1)


        val pessoaDao = AppDatabase.getInstance(this).pessoaDao()

        var pessoa = Pessoa()


        doAsync {
            pessoa = pessoaDao.getPessoaById(idEmpregado)

            uiThread {
                tvNomeDetalheEmpregado.text = "${pessoa.nome}"
                tvEmailDetalheEmpregado.text = "${pessoa.email}"
                edtCargoDetalheEmpregado.setText(pessoa.cargo)
                edtHorasSemanaisDetalheEmpregado.setText(pessoa.horasSemanais)


            }
        }

        spinnerLocalidadeDetalhe.adapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, myStrings)
        spinnerLocalidadeDetalhe.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                localidade = myStrings[position]


            }


        }

        btnAlterarEmpregado.setOnClickListener{

            val pessoaDao = AppDatabase.getInstance(this).pessoaDao()

            var pessoa = Pessoa()




            doAsync {

                if (pessoaDao.getPessoaByEmail(tvEmailDetalheEmpregado.text.toString()) != null){
                    pessoa = pessoaDao.getPessoaByEmail(tvEmailDetalheEmpregado.text.toString())

                    uiThread {



                        pessoa.localidade = localidade
                        pessoa.cargo = edtCargoDetalheEmpregado.text.toString()
                        pessoa.horasSemanais = edtHorasSemanaisDetalheEmpregado.text.toString()





                        doAsync {

                            pessoaDao.insert(pessoa)

                            uiThread {

                                finish()

                            }

                        }

                    }

                }



            }

        }


    }
}
