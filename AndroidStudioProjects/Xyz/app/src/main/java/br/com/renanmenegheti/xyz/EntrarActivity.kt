package br.com.renanmenegheti.xyz

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_entrar.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class EntrarActivity : AppCompatActivity() {




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_entrar)



        btnCadastrar.setOnClickListener{




            val intent = Intent(this, CadastroActivity::class.java )
            startActivity(intent)

        }

        btnEntrar.setOnClickListener(){


            val pessoaDao = AppDatabase.getInstance(this).pessoaDao()

            var pessoa= Pessoa()

            doAsync {

                pessoa = pessoaDao.getPessoaByEmail(edtEmailEntrar.text.toString())


                uiThread {

                    if (pessoa.senha == edtSenhaEntrar.text.toString()){

                        Log.e("yikes", "wow")

                        val intent = Intent(it, WorkspacesJuridicaActivity::class.java)
                        intent.putExtra("idPessoa", pessoa.idPessoa)
                        startActivity(intent)

                    } else{
                        Log.e("yikes", "aee")
                        Toast.makeText(it, "Senha incorreta", Toast.LENGTH_LONG).show()
                    }

                }
            }
        }

        btnDebug.setOnClickListener(){

            val pessoaDao = AppDatabase.getInstance(this).pessoaDao()

            var pessoas = listOf<Pessoa>()

            val workspaceDao = AppDatabase.getInstance(this).workspaceDao()

            var workspaces = listOf<Workspace>()

            doAsync {

                pessoas = pessoaDao.getAll()


                uiThread {



                        Log.e("all pessoas", pessoas.toString())

                        doAsync {
                            workspaces = workspaceDao.getAll()
                            uiThread {
                                Log.e("all workspaces", workspaces.toString())
                            }
                        }





                }
            }


        }

    }









    fun onDeleteAllPessoas(){

        val pessoaDao = AppDatabase.getInstance(this).pessoaDao()



        doAsync {
            pessoaDao.deleteAll()

            uiThread {


            }
        }

    }
}
