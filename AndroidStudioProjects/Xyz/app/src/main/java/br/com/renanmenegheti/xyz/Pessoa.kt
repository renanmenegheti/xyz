package br.com.renanmenegheti.xyz

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "Pessoa")
data class Pessoa(@PrimaryKey(autoGenerate = true) var idPessoa: Int = 0,
                  var nome: String = "", var email: String = "", var senha: String= "", var tipo: String = "",
                  var idWorkspaceQueTrabalha: Int = 0, var cargo: String = "", var horasSemanais: String = "0",
                  var localidade: String = "")