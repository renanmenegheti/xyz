package br.com.renanmenegheti.xyz

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface PessoaDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(pessoa: Pessoa)

    @Query("SELECT * FROM Pessoa")
    fun getAll(): List<Pessoa>

    @Query("SELECT * FROM Pessoa WHERE idWorkspaceQueTrabalha = :idWorkspace")
    fun getAllEmpregadosByIdWorkspace(idWorkspace: Int): List<Pessoa>

    @Query("DELETE FROM Pessoa")
    fun deleteAll()

    @Query("SELECT * FROM Pessoa WHERE email = :emailParametro LIMIT 1")
    fun getPessoaByEmail(emailParametro: String): Pessoa

    @Query("SELECT * FROM Pessoa WHERE idPessoa = :id LIMIT 1")
    fun getPessoaById(id: Int): Pessoa

    @Query("SELECT * FROM Pessoa WHERE idWorkspaceQueTrabalha = :idWorkspace")
    fun getEmpregadosDeUmWorkspace(idWorkspace: Int): List<Pessoa>





}