package br.com.renanmenegheti.xyz

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_ver_empregados.view.*
import kotlinx.android.synthetic.main.empregados_item_lista.view.*
import kotlinx.android.synthetic.main.workspace_item_lista.view.*

class VerEmpregadosAdapter(val context: Context, val empregados: List<Pessoa>): RecyclerView.Adapter<VerEmpregadosAdapter.ViewHolder>() {


    var itemClickListener: ((index: Int) -> Unit)? = null

    fun setOnClickListener(clique: ((index: Int) -> Unit)){
        this.itemClickListener = clique
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.empregados_item_lista,parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return empregados.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(context, empregados[position], itemClickListener)
    }



    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bindView(context: Context, empregado: Pessoa, itemClickListener: ((index: Int) -> Unit)?) {
            itemView.tvNomeEmpregadoWorkspace.text = empregado.nome
            itemView.tvCargoEmpregadoWorkspace.text = empregado.cargo

//            GlideApp.with(context)
//                .load(drink.strDrinkThumb)
//                .placeholder(R.mipmap.ic_broken_image)
//                .centerCrop()
//                .into(itemView.imgDrink)

            if(itemClickListener != null) {
                itemView.setOnClickListener {
                    itemClickListener.invoke(adapterPosition)
                }
            }

        }

    }
}