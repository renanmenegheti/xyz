package br.com.renanmenegheti.xyz

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_ver_empregados.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class VerEmpregadosJuridicaActivity : AppCompatActivity() {

    var idWorkspaceGlobal = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ver_empregados)


        val idWorkspace = intent.getIntExtra("idWorkspace", -1)

        idWorkspaceGlobal = idWorkspace

        val workspaceDao = AppDatabase.getInstance(this).workspaceDao()

        var workspace = Workspace()

        doAsync {
            workspace = workspaceDao.getWorkspaceById(idWorkspace)

            uiThread {
                tvEmpregadosDeWorkspace.text = "Empregados de ${workspace.nomeWorkspace}"
            }
        }



        btnAddEmpregadoJuridica.setOnClickListener{


            val intent = Intent(this, CadastrarEmpregadoActivity::class.java)
            intent.putExtra("idWorkspace", idWorkspace)
            startActivityForResult(intent, 13)

        }

    }

    override fun onResume() {
        super.onResume()

        atualizaLista()
    }

    private fun atualizaLista() {
        val pessoaDao = AppDatabase.getInstance(this).pessoaDao()

        doAsync {
            val empregados = pessoaDao.getAllEmpregadosByIdWorkspace(idWorkspaceGlobal)

            uiThread {

                val adapter = VerEmpregadosAdapter(it, empregados)
                adapter.setOnClickListener {position ->
                    val abrirActivityDetalheEmpregado = Intent(it, DetalheEmpregadoActivity::class.java)
                    abrirActivityDetalheEmpregado.putExtra("idPessoa", empregados[position].idPessoa)
                    startActivityForResult(abrirActivityDetalheEmpregado, 1298)
                }

                rvVerEmpregadosJuridica.adapter = adapter
                rvVerEmpregadosJuridica.layoutManager = LinearLayoutManager(it)

            }
        }
    }
}
