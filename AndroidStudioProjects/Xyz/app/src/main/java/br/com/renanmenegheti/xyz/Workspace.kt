package br.com.renanmenegheti.xyz

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(foreignKeys = [
    ForeignKey(
        entity = Pessoa::class,
        parentColumns = ["idPessoa"],
        childColumns = ["donoWorkspace"]
    )]
)
data class Workspace(@PrimaryKey(autoGenerate = true) var idWorkspace: Int = 0, var donoWorkspace: Int = 0,
                     var nomeWorkspace: String = "", var endereco: String = "")