package br.com.renanmenegheti.xyz

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.workspace_item_lista.view.*

class WorkspaceAdapter(val context: Context, val workspaces: List<Workspace>): RecyclerView.Adapter<WorkspaceAdapter.ViewHolder>() {


    var itemClickListener: ((index: Int) -> Unit)? = null

    fun setOnClickListener(clique: ((index: Int) -> Unit)){
        this.itemClickListener = clique
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.workspace_item_lista,parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return workspaces.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(context, workspaces[position], itemClickListener)
    }



    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bindView(context: Context, workspace: Workspace, itemClickListener: ((index: Int) -> Unit)?) {
            itemView.tvNomeWorkspace.text = workspace.nomeWorkspace
            itemView.tvEnderecoWorkspace.text = workspace.endereco

//            GlideApp.with(context)
//                .load(drink.strDrinkThumb)
//                .placeholder(R.mipmap.ic_broken_image)
//                .centerCrop()
//                .into(itemView.imgDrink)

            if(itemClickListener != null) {
                itemView.setOnClickListener {
                    itemClickListener.invoke(adapterPosition)
                }
            }

        }

    }
}