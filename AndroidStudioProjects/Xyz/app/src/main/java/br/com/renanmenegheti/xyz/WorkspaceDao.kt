package br.com.renanmenegheti.xyz

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface WorkspaceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(workspace: Workspace)

    @Query("SELECT * FROM Workspace")
    fun getAll(): List<Workspace>

    @Query("SELECT * FROM Workspace WHERE idWorkspace = :idWorkspace LIMIT 1")
    fun getWorkspaceById(idWorkspace: Int): Workspace

}