package br.com.renanmenegheti.xyz

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_workspaces_juridica.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class WorkspacesJuridicaActivity : AppCompatActivity() {




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_workspaces_juridica)


            var idPessoa = intent.getIntExtra("idPessoa", -1)
            val pessoaDao = AppDatabase.getInstance(this).pessoaDao()

            doAsync {

                val pessoa  = pessoaDao.getPessoaById(idPessoa)


                uiThread {

                    tvDonoWorkspaces.text = "Workspaces de ${pessoa.nome}"

                }
            }










        btnAddWorkspaceJuridica.setOnClickListener(){

            val intent = Intent(this, CriarWorkspaceActivity::class.java)
            intent.putExtra("idPessoa", idPessoa)
            startActivityForResult(intent, 13)

        }
    }


    override fun onResume() {
        super.onResume()

        atualizaLista()

    }

    private fun atualizaLista() {

        val workspaceDao = AppDatabase.getInstance(this).workspaceDao()

        doAsync {
            val workspaces = workspaceDao.getAll()

            uiThread {

                val adapter = WorkspaceAdapter(it, workspaces)
                adapter.setOnClickListener {position ->
                    val abrirActivityVerWorkspace = Intent(it, VerEmpregadosJuridicaActivity::class.java)
                    abrirActivityVerWorkspace.putExtra("idWorkspace", workspaces[position].idWorkspace)
                    startActivity(abrirActivityVerWorkspace)
                }

                rvWorkspacesJuridica.adapter = adapter
                rvWorkspacesJuridica.layoutManager = LinearLayoutManager(it)

            }
        }



    }


}
